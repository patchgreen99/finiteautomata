import numpy as np
from simulation import tasks

p2 = 0.5
p3 = 1
size = 50
results=[]
for p1 in np.arange(0,100,1):
    result = tasks.run.apply_async(args=[size,p1,p2,p3])
    results.append(result)

start = True
for r in results:
    if start:
        start = False
        output = r.wait(timeout=None, interval=0.5)
    else:
        output = np.vstack((output, r.wait(timeout=None, interval=0.5)))

np.save("data.npy", output)

