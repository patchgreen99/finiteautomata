from __future__ import absolute_import

from celery import Celery

app = Celery('backend',
             broker='amqp://guest@localhost//',
             backend='amqp://guest@localhost//',
             include=['simulation.tasks'] #References your tasks. Donc forget to put the whole absolute path.
             )

app.conf.update(
        CELERY_TASK_SERIALIZER = 'json',
        CELERY_RESULT_SERIALIZER = 'pickle',
        CELERY_ACCEPT_CONTENT=['json','pickle'],
        CELERY_TIMEZONE = 'Europe/Oslo',
        CELERY_ENABLE_UTC = True
                )