import numpy as np
from simulation.celery import app


def adjacent(lattice,w,h,size):
    for p in [(1,0),(-1,0),(0,1),(0,-1)]:
        x,y = p
        if lattice[(w+x)%size,(h+y)%size] == 1:
            return True
    return False

def build_lattice(size):
    return np.random.randint(0, 3, (size, size))

def infected(lattice):
    cur = np.copy(lattice)
    cur[cur==2] = 0
    return np.average(cur)

@app.task
def run(size, p1, p2, p3):
    def animate(data, im):
        im.set_data(data)

    def mcStep(size, p1, p2, p3):
        for step in range(size * size):
            h = np.random.randint(0, size)
            w = np.random.randint(0, size)

            r = np.random.rand()
            if lattice[w, h] == 1:
                if p2 > r:
                    lattice[w, h] = 2
            elif lattice[w, h] == 2:
                if p3 > r:
                    lattice[w, h] = 0
            elif lattice[w, h] == 0:
                if p1 > r and adjacent(lattice, w, h, size):
                    lattice[w, h] = 1

                    # yield lattice

    def batchStep(size, p1, p2, p3):
        # S-0 I-1 R-2
        cur = np.copy(lattice)

        up = np.vstack([cur[1:, :] + cur[:-1, :], (cur[0, :] + cur[-1, :]).reshape(1, -1)])
        down = np.vstack([(cur[0, :] + cur[-1, :]).reshape(1, -1), cur[1:, :] + cur[:-1, :]])

        left = np.hstack([cur[:, :1] + cur[:, :-1], (cur[:, 0] + cur[:, -1]).reshape(-1, 1)])
        right = np.hstack([(cur[:, 0] + cur[:, -1]).reshape(-1, 1), cur[:, :1] + cur[:, :-1]])

        neighbours = np.dstack([up, down, left, right])
        neighbours[neighbours > 1] = 0

        s = np.argwhere(np.sum(neighbours, axis=2) <> 0)
        np.random.shuffle(s)
        s = s[:s.shape[0] * p1]
        lattice[s[:, 0], s[:, 1]] = 1

        i = np.argwhere(cur == 1)
        np.random.shuffle(i)
        i = i[:i.shape[0] * p2]
        lattice   [i[:, 0], i[:, 1]] = 2

        r = np.argwhere(cur == 2)
        np.random.shuffle(r)
        r = r[:r.shape[0] * p3]
        lattice[r[:, 0], r[:, 1]] = 0

        # yield lattice


    for p3 in np.arange(0, 100, 1):
        lattice = build_lattice(size)

        """
        Experiment
        """
        measurements = 10
        step = lambda : mcStep(size,p1*0.01,p2*0.01,p3*0.01)
        for pad in range(10):
            step()

        measurement = 0
        for m in range(measurements):
            for pad in range(10):
                step()
            measurement += infected(lattice)
        try:
            output = np.vstack((output, np.array([p1, p2, p3, measurement / measurements])))
        except:
            output = np.array([p1, p2, p3, measurement / measurements])

    return output








