import os
from multiprocessing import Pool
from name import machines

def f(num):
    try:
        a  = os.popen('ssh -q -T cplab%03d'%num+' "python /home/s1308424/finiteautomata/thread.py '+str(num)+'; exit"').readlines()

        if len(a) != 0:
            for item in a:
                print item.strip()
    except:
        pass

p = Pool(169)
p.map(f, [r for r in range(169)])
