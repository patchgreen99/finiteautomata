import sys
from multiprocessing import Pool
import numpy as np


def adjacent(lattice,w,h,size):
    for p in [(1,0),(-1,0),(0,1),(0,-1)]:
        x,y = p
        if lattice[(w+x)%size,(h+y)%size] == 1:
            return True
    return False

def build_lattice(size,immune):
    # immune make a 4th state that acts like R
    l = np.random.randint(0, 2, (size, size))
    allpoints = np.argwhere(l <> 3)
    np.random.shuffle(allpoints)
    allpoints = allpoints[:size * size * immune]
    l[allpoints[:, 0], allpoints[:, 1]] = 3
    return l

def infected(lattice):
    # immune make a 4th state that acts like R
    cur = np.copy(lattice)
    cur[cur>1] = 0
    return np.average(cur)

def run(size, p1, p2, p3, immune):
    def animate(data, im):
        im.set_data(data)

    def mcStep(size, p1, p2, p3):
        for step in range(size * size):
            h = np.random.randint(0, size)
            w = np.random.randint(0, size)

            r = np.random.rand()
            if lattice[w, h] == 1:
                if p2 > r:
                    lattice[w, h] = 2
            elif lattice[w, h] == 2:
                if p3 > r:
                    lattice[w, h] = 0
            elif lattice[w, h] == 0:
                if p1 > r and adjacent(lattice, w, h, size):
                    lattice[w, h] = 1

                    # yield lattice

    def batchStep(size, p1, p2, p3):
        # S-0 I-1 R-2
        cur = np.copy(lattice)

        up = np.vstack([cur[1:, :] + cur[:-1, :], (cur[0, :] + cur[-1, :]).reshape(1, -1)])
        down = np.vstack([(cur[0, :] + cur[-1, :]).reshape(1, -1), cur[1:, :] + cur[:-1, :]])

        left = np.hstack([cur[:, :1] + cur[:, :-1], (cur[:, 0] + cur[:, -1]).reshape(-1, 1)])
        right = np.hstack([(cur[:, 0] + cur[:, -1]).reshape(-1, 1), cur[:, :1] + cur[:, :-1]])

        neighbours = np.dstack([up, down, left, right])
        neighbours[neighbours > 1] = 0

        s = np.argwhere(np.sum(neighbours, axis=2) <> 0)
        np.random.shuffle(s)
        s = s[:s.shape[0] * p1]
        lattice[s[:, 0], s[:, 1]] = 1

        i = np.argwhere(cur == 1)
        np.random.shuffle(i)
        i = i[:i.shape[0] * p2]
        lattice[i[:, 0], i[:, 1]] = 2

        r = np.argwhere(cur == 2)
        np.random.shuffle(r)
        r = r[:r.shape[0] * p3]
        lattice[r[:, 0], r[:, 1]] = 0

        # yield lattice


    lattice = build_lattice(size, immune)

    """
    Experiment
    """
    measurements = 10
    for pad in range(10):
        mcStep(size,p1*0.01,p2*0.01,p3*0.01)

    measurement = 0
    measurementsqd = 0
    for m in range(measurements):
        for pad in range(10):
            mcStep(size,p1*0.01,p2*0.01,p3*0.01)
        measurement += infected(lattice)
        measurementsqd += infected(lattice)**2

    print str([p1,p2,p3, measurement/measurements, measurementsqd/measurements])




num = int(sys.argv[1])

def f(sub):
    # Create 2D linspace
    val =num+sub*169
    a = np.zeros((100,100))
    xy = np.argwhere(a==0)
    p2 = 50
    p3 = 50
    size = 50

    for exp in range(100):
        p1 = xy[(val*100+exp)%10000, 0]
        immune = xy[(val*100+exp)%10000, 1]
        run(size,p1,p2,p3,float(immune)/100.0)

p = Pool(4)
p.map(f, [r for r in range(4)])
