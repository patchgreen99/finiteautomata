import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation
from matplotlib.widgets import Slider
import argparse

def adjacent(lattice,w,h,size):
    for p in [(1,0),(-1,0),(0,1),(0,-1)]:
        x,y = p
        if lattice[(w+x)%size,(h+y)%size] == 1:
            return True
    return False

def build_lattice(size, immune):
    # immune make a 4th state that acts like R
    l = np.random.randint(0, 2, (size, size))
    allpoints = np.argwhere(l <> 3)
    np.random.shuffle(allpoints)
    allpoints = allpoints[:size*size*immune]
    l[allpoints[:,0],allpoints[:,1]] = 3
    return l

def simulation(args):
    size = args.size
    lattice = build_lattice(size, args.immune)
    fig, ax = plt.subplots()

    def animate(data, im):
        im.set_data(data)

    def mcStep():
        for step in range(size * size):
            h = np.random.randint(0, size)
            w = np.random.randint(0, size)

            r = np.random.rand()
            if lattice[w,h]==1:
                if args.p2 > r:
                    lattice[w, h] = 2
            elif lattice[w,h]==2:
                if args.p3 > r:
                    lattice[w, h] = 0
            elif lattice[w, h] == 0:
                if args.p1 > r and adjacent(lattice, w, h, size):
                    lattice[w, h] = 1

        yield lattice

    def batchStep():
        # S-0 I-1 R-2
        cur = np.copy(lattice)

        up=np.vstack([cur[1:, :]+cur[:-1, :],(cur[0,:]+cur[-1,:]).reshape(1,-1)])
        down=np.vstack([(cur[0,:]+cur[-1,:]).reshape(1,-1),cur[1:, :]+cur[:-1, :]])

        left = np.hstack([cur[:, :1]+cur[:, :-1], (cur[:,0]+cur[:,-1]).reshape(-1,1)])
        right = np.hstack([(cur[:,0]+cur[:,-1]).reshape(-1,1), cur[:, :1]+cur[:, :-1]])

        neighbours = np.dstack([up,down,left,right])
        neighbours[neighbours>1]=0

        s = np.argwhere(np.sum(neighbours,axis=2)<>0)
        np.random.shuffle(s)
        s = s[:s.shape[0] * args.p1]
        lattice[s[:,0],s[:,1]] = 1

        i = np.argwhere(cur == 1)
        np.random.shuffle(i)
        i = i[:i.shape[0] * args.p2]
        lattice[i[:,0],i[:,1]] = 2

        r = np.argwhere(cur == 2)
        np.random.shuffle(r)
        r= r[:r.shape[0] * args.p3]
        lattice[r[:,0],r[:,1]] = 0

        yield lattice

    for l in mcStep():
        im = ax.imshow(l, interpolation='nearest')

    plt.colorbar(im)

    ani = animation.FuncAnimation(
        fig, animate, mcStep, interval=1, repeat=True, fargs=(im,))


    plt.show()

parser = argparse.ArgumentParser(description='Ising model simulation')
parser.add_argument('-n', action="store", dest="size", type=int, help="the square root of the number of spins to simulate", default=100)
parser.add_argument('-p1', action="store", dest="p1", type=str, help="S -> I probability", default=0.95)
parser.add_argument('-p2', action="store", dest="p2", type=str, help="I -> R probability", default=0.5)
parser.add_argument('-p3', action="store", dest="p3", type=float, help="R -> S probability",default=0.05)
parser.add_argument('-im', action="store", dest="immune", type=float, help="Fraction immune",default=0.1)
args = parser.parse_args()
simulation(args)